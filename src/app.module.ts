import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { OrderModule } from './order/order.module';
import { ContactModule } from './contact/contact.module';
import { CommentModule } from './comment/comment.module';
import { ProductModule } from './product/product.module';
import { TypeOrmModule } from '@nestjs/typeorm';


@Module({
  imports: [TypeOrmModule.forRoot(), UsersModule, OrderModule, ContactModule, CommentModule, ProductModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

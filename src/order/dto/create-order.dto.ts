import { IsString, MinLength, MaxLength } from 'class-validator';

export class CreateOrderDto {
  @IsString()
  @MinLength(2)
  @MaxLength(100)
  quantity: string;
}

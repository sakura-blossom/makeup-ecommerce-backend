import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class CreateUserDto {
  @IsEmail()
  @ApiProperty({ type: String, description: 'email' })
  email: string;

  @IsString()
  @IsOptional()
  @MinLength(6)
  @MaxLength(20)
  @ApiProperty({ type: String, description: 'password' })
  password: string;

  @IsString()
  @MinLength(2)
  @MaxLength(60)
  @ApiProperty({ type: String, description: 'username' })
  username: string;
}

import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  Entity,
} from 'typeorm';

@Entity()
export class Contact {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  email: string;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'userId' })
  user: User;

  @Column()
  message_object: string;
  @Column()
  phone: string;
  @Column()
  address: string;

  @Column()
  message: string;

  @CreateDateColumn({ select: false })
  createdAt: Date;
}

import {
  IsEmail,
  IsString,
  MaxLength,
  MinLength,
  IsPhoneNumber,
} from 'class-validator';

export class CreateContactDto {
  @IsEmail()
  email: string;

  @IsString()
  @MinLength(2)
  @MaxLength(60)
  message_object: string;

  @IsString()
  @MinLength(2)
  @MaxLength(60)
  message: string;

  @IsPhoneNumber()
  @MinLength(2)
  @MaxLength(60)
  phone: string;

  @IsString()
  @MinLength(2)
  @MaxLength(60)
  address: string;
}

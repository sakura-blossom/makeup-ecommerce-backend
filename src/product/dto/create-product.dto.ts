import { IsString, MaxLength, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateProductDto {
  @IsString()
  @MaxLength(100)
  @ApiProperty({ type: String, description: 'product_name' })
  product_name: string;

  @IsOptional()
  @IsString()
  @MaxLength(20)
  @ApiProperty({ type: String, description: 'product_code' })
  product_code: string;

  @IsOptional()
  @IsString()
  @MaxLength(20)
  @ApiProperty({ type: String, description: 'product_price' })
  product_price: string;

  @IsOptional()
  @IsString()
  @MaxLength(20)
  @ApiProperty({ type: String, description: 'product_desc' })
  product_desc: string;
}

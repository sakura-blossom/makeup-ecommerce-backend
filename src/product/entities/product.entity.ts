import { Column, PrimaryGeneratedColumn, Entity } from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  product_name: string;
  @Column()
  product_code: string;
  @Column()
  product_desc: string;
  @Column()
  product_price: string;

  constructor(partial: Partial<Product>) {
    Object.assign(this, partial);
  }
}
